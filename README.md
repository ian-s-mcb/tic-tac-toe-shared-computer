# tic-tac-toe-shared-computer

A tic-tac-toe game that two users can play on a shared computer.

This project was guided by this [freeCodeCamp.org/Scrimba.com tutorial][tutorial].

### [Demo][demo]

### Getting started
```bash
cd tic-tac-toe-shared-computer
yarn
yarn start
# Navigate to http://localhost:3000
```

### Technologies used
* [React][react] (and [React Hooks][react-hooks])
* [create-react-app][create-react-app]
* [Heroku][heroku]

### Screenshots
* 'X' wins this round
![screenshot-1][screenshot-1]
* Use move history to rewind so that player 'O' ties instead of loses
![screenshot-2][screenshot-2]

[tutorial]: https://www.freecodecamp.org/news/learn-how-to-build-tic-tac-toe-with-react-hooks/
[demo]: https://t3-shared-computer.netlify.app/
[react]: reactjs.org/
[react-hooks]: https://reactjs.org/docs/hooks-intro.html
[create-react-app]: https://github.com/facebook/create-react-app
[heroku]: https://www.heroku.com/
[screenshot-1]: https://i.imgur.com/67Czv1a.gif
[screenshot-2]: https://i.imgur.com/3HPJoY0.gif
