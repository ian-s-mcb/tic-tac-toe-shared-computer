import React from 'react'

import Footer from './components/Footer'
import Game from './components/Game'
import Header from './components/Header'

const App = () => (
  <>
    <Header />
    <Game />
    <Footer />
  </>
)

export default App
