import React from 'react'

const styles = {textAlign: 'center'}

const Header = () => (
  <h1 style={styles}>t3-shared-computer</h1>
)

export default Header
