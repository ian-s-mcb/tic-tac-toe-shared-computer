import React from 'react'

const styles = { textAlign: 'center'}

const Footer = () => (
  <p style={styles}>
    Created by <a href="https://gitlab.com/ian-s-mcb">ian-s-mcb</a>
    <br />
    by following this
    <br />
    <a href="https://www.freecodecamp.org/news/learn-how-to-build-tic-tac-toe-with-react-hooks/">freeCodeCamp.org/Scrimba.com tutorial</a>
  </p>
)

export default Footer
